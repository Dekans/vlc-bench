<resources>
    <string name="app_name" translatable="false">VLC Benchmark</string>
    <string name="upload_results">Upload</string>
    <string name="default_percent_value">0 %</string>
    <string name="no_results">No result found, please run a test.</string>

    <!-- page names -->
    <string name="settings_page">Settings</string>
    <string name="results_page">Results</string>

    <!-- bottom_navigation_bar -->
    <string name="home_nav">Home</string>
    <string name="result_nav">Results</string>
    <string name="settings_nav">Settings</string>

    <!-- toolbar titles -->
    <string name="title_home">VLC Benchmark</string>
    <string name="title_results">Results</string>
    <string name="title_settings">Settings</string>

    <!-- settings -->
    <string name="general_pref_category">General</string>
    <string name="misc_pref_category">Misc</string>
    <string name="about_pref">About</string>
    <string name="delete_saves_pref">Delete benchmarks</string>
    <string name="delete_saves_key">delete_saves_key</string>
    <string name="delete_samples_pref">Delete video samples</string>
    <string name="delete_samples_key">delete_samples_key</string>
    <string name="connect_pref">Connect to Google account</string>
    <string name="connect_key">connect_key</string>
    <string name="disconnect_pref">Disconnect from Google account</string>
    <string name="disconnect_key">disconnect_key</string>

    <!-- About content -->
    <string name="about_description">VLCBenchmark is a tool to benchmark android devices\' video capabilities using the VLC Media Player.</string>
    <string name="about_copyright" translatable="false">Copyleft &#169; 2016&#8211;2017 by VideoLAN.</string>
    <string name="about_authors" translatable="false">Jean-Baptiste&#160;Kempf, Geoffrey&#160;Métais, Alexandre&#160;Perraud, Thomas&#160;Guillem, Benoit&#160;Du&#160;Payrat, Bastien&#160;Penavayre, Duncan&#160;McNamara</string>
    <string name="about_revision">Revision:</string>
    <string name="about_compiled_by">This VLCBenchmark version is compiled by:</string>
    <string name="about_link">http://www.videolan.org</string>
    <string name="about_vlc_min">Vlc min version: %1$s</string>
    <!-- About tabs -->
    <string name="tab_about">About</string>
    <string name="tab_licence">Licence</string>

    <!-- dialog titles -->
    <string name="dialog_title_error">Error</string>
    <string name="dialog_title_warning">Warning</string>
    <string name="dialog_title_oups">Oups &#8230;</string>
    <string name="dialog_title_missing_vlc">Missing VLC</string>
    <string name="dialog_title_outdated_vlc">Outdated VLC</string>
    <string name="dialog_title_testing">Testing &#8230;</string>
    <string name="dialog_title_file_deletion">File deletion</string>
    <string name="dialog_title_sample_deletion">Sample deletion</string>
    <string name="dialog_title_downloading">Downloading &#8230;</string>

    <!-- dialog texts -->
    <string name="dialog_text_no_internet">"Cannot connect to the internet, please turn wifi on and retry."</string>
    <string name="dialog_text_sample">There was a problem while checking samples</string>
    <string name="dialog_text_err_google">There was a problem while connecting to Google</string>
    <string name="dialog_text_downloading">Downloading &#8230;</string>
    <string name="dialog_text_testing">Testing &#8230;</string>
    <string name="dialog_text_download_error">There was an error during download</string>
    <string name="dialog_text_missing_vlc">You need the VLC Media Player to start a benchmark.\nPlease install it to continue.</string>
    <string name="dialog_text_outdated_vlc">You need the latest version of VLC Media Player to start a benchmark.\nPlease update it to continue.</string>
    <string name="dialog_text_oups">There was an unexpected problem</string>
    <string name="dialog_text_battery_warning">You only have %1$d %% of battery charge left, you should plug your phone</string>
    <string name="dialog_text_file_deletion_success">Deleted all benchmark results</string>
    <string name="dialog_text_file_deletion_failure">Failed to delete all benchmark results</string>
    <string name="dialog_text_sample_deletion_success">Deleted all test samples</string>
    <string name="dialog_text_sample_deletion_failure">Failed to delete all test samples</string>
    <string name="dialog_text_deletion_confirmation">Are you sure you want to delete these files ?</string>
    <string name="dialog_text_save_failure">There was a problem while saving the benchmark results.</string>
    <string name="dialog_text_file_creation_failure">There was a problem while creating files</string>
    <string name="dialog_text_loading_results_failure">There was a problem while loading results</string>

    <!-- dialog buttons -->
    <string name="dialog_btn_continue">Continue</string>
    <string name="dialog_btn_cancel">Cancel</string>
    <string name="dialog_btn_ok">OK</string>

    <!-- main page -->
    <string name="specs_title">DEVICE</string>
    <string name="specs_model">Model</string>
    <string name="specs_android">Android</string>
    <string name="specs_cpu">CPU</string>
    <string name="specs_cpuspeed">CPU Speed</string>
    <string name="specs_memory">Memory</string>
    <string name="explanation_title">BENCHMARK</string>
    <string name="explanation_text">
        This benchmark will test your device\'s video capabilities using the VLC Media Player.\nYou can start a benchmark with the left button below, or start 3 and get the average results with the right button below.
    </string>

    <string name="missing_samples">Missing test samples.</string>
    <string name="download_warning">
        To perform a benchmark, some test samples need to be downloaded.\nYou need to be connected to the wifi in order to download them.\nPlease note it could take some time and storage space, as there may be many.\nFurthermore, the said list is likely to evolve.
    </string>

    <string name="dialog_text_starting">Starting benchmark</string>
    <string name="progress_text_format">%1$s %% | file %2$d/%3$d | test %4$d</string>
    <string name="progress_text_format_loop">%1$s %% | file %2$d/%3$d | test %4$d | loop %5$d/%6$d</string>

    <!-- result list -->
    <string name="score">Score: </string>
    <string name="result_score">%1$s / %2$s</string>

    <!-- result detail -->
    <string name="detail_software">Software</string>
    <string name="detail_hardware">Hardware</string>
    <string name="detail_score">Score: %1$s</string>
    <string name="detail_bad_screenshots">Percent of bad screenshots: %1$s %%</string>
    <string name="detail_frames_dropped">Frames dropped: %1$d</string>
    <string name="detail_warning_number">Number of warnings: %1$d</string>

</resources>
