/*****************************************************************************
 * Util.java
 *****************************************************************************
 * Copyright © 2017 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tools;

import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Util {

    private final static String TAG = Util.class.getName();

    public static ExecutorService mThreadPool = Executors.newSingleThreadExecutor();
    public static Handler mHandler = new Handler(Looper.getMainLooper());

    public static String readAsset(String assetName, AssetManager assetManager) {
        InputStream is = null;
        BufferedReader r = null;
        try {
            is = assetManager.open(assetName);
            r = new BufferedReader(new InputStreamReader(is, "UTF8"));
            StringBuilder sb = new StringBuilder();
            String line = r.readLine();
            if(line != null) {
                sb.append(line);
                line = r.readLine();
                while(line != null) {
                    sb.append('\n');
                    sb.append(line);
                    line = r.readLine();
                }
            }
            return sb.toString();
        } catch (IOException e) {
            return null;
        } finally {
            close(is);
            close(r);
        }
    }

    private static boolean close(Closeable closeable) {
        if (closeable != null)
            try {
                closeable.close();
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Failed to close: " + e.toString());
            }
        return false;
    }

    public static void runInUiThread(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static void runInBackground(Runnable runnable) {
        mThreadPool.execute(runnable);
    }
}
